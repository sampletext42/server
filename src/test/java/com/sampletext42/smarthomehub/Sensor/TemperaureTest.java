package com.sampletext42.smarthomehub.Sensor;

import static org.junit.jupiter.api.Assertions.*;

import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Mock_PubSubClient;
import org.junit.jupiter.api.Test;

/**
 * Class for testing Temperature
 */
public class TemperaureTest {

    /**
     * Testing method
     */
    @Test
    public void test_Temperature() throws PubSubClientException {
        Temperature cut = new Temperature(3, new Mock_PubSubClient());

        // Test MQTT-Callback
        cut.changeCallback("/Sensors/Sensor_003", "0.1337"); // inject sensor message
        assertEquals(0.1337, cut.getValue(), 1E-5);
    }
}
