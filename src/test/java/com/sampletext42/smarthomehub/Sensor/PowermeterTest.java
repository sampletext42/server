package com.sampletext42.smarthomehub.Sensor;

import static org.junit.jupiter.api.Assertions.*;

import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Mock_PubSubClient;
import org.junit.jupiter.api.Test;

/**
 * Class for testing Powermeter
 */
public class PowermeterTest {

    /**
     * Testing method
     */
    @Test
    public void test_Powermeter() throws PubSubClientException {
        Powermeter cut = new Powermeter(2, new Mock_PubSubClient());

        // Test MQTT-Callback
        cut.changeCallback("/Sensors/Sensor_002", "0.1337");
        assertEquals(0.1337, cut.getValue(), 1E-5);
    }

}
