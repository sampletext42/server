package com.sampletext42.smarthomehub.Sensor;

import static org.junit.jupiter.api.Assertions.*;

import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Mock_PubSubClient;
import org.junit.jupiter.api.Test;

/**
 * Class for testing Humidity
 */
public class HumidityTest {

    /**
     * Testing method
     */
    @Test
    public void test_Humidity() throws PubSubClientException {
        Humidity testHumid = new Humidity(1, new Mock_PubSubClient());

        // Test MQTT-Callback
        testHumid.changeCallback("/Sensors/Sensor_001", "0.1337");
        assertEquals(0.1337, testHumid.getValue(), 1E-5);
    }

}
