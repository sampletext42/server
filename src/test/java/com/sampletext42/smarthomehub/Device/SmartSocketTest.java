package com.sampletext42.smarthomehub.Device;

import static com.sampletext42.smarthomehub.Device.BaseDevice.PowerState.OFF;
import static com.sampletext42.smarthomehub.Device.BaseDevice.PowerState.ON;
import static org.junit.jupiter.api.Assertions.*;

import com.sampletext42.smarthomehub.Actuator.Relay;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Mock_PubSubClient;
import com.sampletext42.smarthomehub.Sensor.Powermeter;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.junit.jupiter.api.Test;

/**
 * Class for testing SmartSocket
 */
public class SmartSocketTest {


    /**
     * Testing method
     */
    @Test
    public void test_SmartSocket() throws PubSubClientException {
        Relay relay = new Relay(1, new Mock_PubSubClient());
        Powermeter power = new Powermeter(2, new Mock_PubSubClient());

        SmartSocket cut = new SmartSocket(1, relay, power);

        // Test if On/Off functionality works
        cut.setOn();
        assertEquals(ON, cut.getPowerState());
        assertEquals(relay.getValue(), 1.0);

        cut.setOff();
        assertEquals(OFF, cut.getPowerState());
        assertEquals(relay.getValue(), 0.0);

        power.changeCallback("/Sensors/Sensor_002", "42.0");
        assertEquals(cut.getPowerUsage(), 42.0);

        assertEquals("DeviceIO{powerState=OFF, id=1, type='SmartSocket', sensors=[Sensor(2) = 42.0W (Powermeter)], actuators=[Actuator(1) = 0.0 (Relay)]}", cut.toString());
    }
}
