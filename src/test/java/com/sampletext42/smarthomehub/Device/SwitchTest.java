package com.sampletext42.smarthomehub.Device;

import com.sampletext42.smarthomehub.Device.BaseDevice.PowerState;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Mock_PubSubClient;
import com.sampletext42.smarthomehub.Sensor.SwitchContact;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Class for testing Lamp
 */
public class SwitchTest {

    /**
     * Testing method
     * @throws SQLException
     */
    @Test
    public void test_Switch() throws PubSubClientException {
        SwitchContact switchContact = new SwitchContact(1, new Mock_PubSubClient());
        Switch cut = new Switch(1, switchContact);

        // Test if On/Off functionality works
        switchContact.changeCallback("/Sensors/Sensor_001", "1.0");
        System.out.println(cut.getPowerState());
        assertEquals(PowerState.ON, cut.getPowerState());
        switchContact.changeCallback("/Sensors/Sensor_001", "0.0");
        System.out.println(cut.getPowerState());
        assertEquals(PowerState.OFF, cut.getPowerState());

        assertEquals("DeviceIO{powerState=OFF, id=1, type='Switch', sensors=[Sensor(1) = 0.0I/O (SwitchContact)], actuators=[]}", cut.toString());
    }

}
