package com.sampletext42.smarthomehub.Device;

import java.sql.SQLException;

import static com.sampletext42.smarthomehub.Device.BaseDevice.PowerState.OFF;
import static com.sampletext42.smarthomehub.Device.BaseDevice.PowerState.ON;
import static org.junit.jupiter.api.Assertions.*;

import com.sampletext42.smarthomehub.Actuator.Light;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Mock_PubSubClient;
import org.junit.jupiter.api.Test;

/**
 * Class for testing Lamp
 */
public class LampTest {

    /**
     * Testing method
     * @throws SQLException
     */
    @Test
    public void test_Lamp() throws PubSubClientException {
        Light light = new Light(1, new Mock_PubSubClient());
        Lamp cut = new Lamp(1, light);

        // Test if On/Off functionality works
        cut.setOn();
        assertEquals(ON, cut.getPowerState());
        assertEquals(light.getValue(), 1.0);
        cut.setOff();
        assertEquals(OFF, cut.getPowerState());;
        assertEquals(light.getValue(), 0.0);

        assertEquals("DeviceIO{powerState=OFF, id=1, type='Lamp', sensors=[], actuators=[Actuator(1) = 0.0 (Light)]}", cut.toString());
    }

}
