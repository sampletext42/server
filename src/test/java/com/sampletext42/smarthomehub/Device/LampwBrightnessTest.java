package com.sampletext42.smarthomehub.Device;

import static com.sampletext42.smarthomehub.Device.BaseDevice.PowerState.OFF;
import static com.sampletext42.smarthomehub.Device.BaseDevice.PowerState.ON;
import static org.junit.jupiter.api.Assertions.*;

import com.sampletext42.smarthomehub.Actuator.LightBrightness;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Mock_PubSubClient;
import org.junit.jupiter.api.Test;

/**
 * Class for testing LampwBrightness
 */
public class LampwBrightnessTest {

    /**
     * Testing method
     */
    @Test
    public void test_LampwBrightness() throws PubSubClientException {
        LightBrightness light = new LightBrightness(1, new Mock_PubSubClient());
        LampwBrightness cut = new LampwBrightness(1, light);

        cut.setOn();
        assertEquals(ON, cut.getPowerState());
        assertEquals(light.getValue(), 1.0);

        // check if brightness works
        cut.setBrightness(0.1337);
        assertEquals(0.1337, cut.getBrightness(), 1E-5);
        assertEquals(light.getValue(), 0.1337, 1E-5);



        cut.setOff();
        assertEquals(OFF, cut.getPowerState());
        assertEquals(light.getValue(), 0.0);
    }

}
