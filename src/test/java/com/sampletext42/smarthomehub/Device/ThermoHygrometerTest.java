package com.sampletext42.smarthomehub.Device;

import static org.junit.jupiter.api.Assertions.*;

import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Mock_PubSubClient;
import com.sampletext42.smarthomehub.Sensor.Humidity;
import com.sampletext42.smarthomehub.Sensor.Temperature;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

/**
 * Class for testing ThermoHygrometer
 */
public class ThermoHygrometerTest {

    /**
     * Testing method
     */
    @Test
    public void test_ThermoHygrometer() throws MqttException, SQLException, PubSubClientException {
        Temperature temp = new Temperature(1, new Mock_PubSubClient());
        Humidity humid = new Humidity(2, new Mock_PubSubClient());
        ThermoHygrometer cut = new ThermoHygrometer(1, temp, humid);

        assertEquals(0.0, cut.getTemperature(), 1E-5);
        assertEquals(0.0, cut.getHumidity(), 1E-5);

        temp.changeCallback("/Sensors/Sensor_001", "-1.0");

        assertEquals(-1.0, cut.getTemperature(), 1E-5);
        assertEquals(0.0, cut.getHumidity(), 1E-5);
        humid.changeCallback("/Sensors/Sensor_002", "1.0");

        assertEquals(-1.0, cut.getTemperature(), 1E-5);
        assertEquals(1.0, cut.getHumidity(), 1E-5);

    }

}
