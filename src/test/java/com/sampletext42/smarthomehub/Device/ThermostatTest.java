package com.sampletext42.smarthomehub.Device;

import static com.sampletext42.smarthomehub.Device.BaseDevice.PowerState.OFF;
import static com.sampletext42.smarthomehub.Device.BaseDevice.PowerState.ON;
import static org.junit.jupiter.api.Assertions.*;

import com.sampletext42.smarthomehub.Actuator.Radiator;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Mock_PubSubClient;
import com.sampletext42.smarthomehub.Sensor.Temperature;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

/**
 * Class for testing Thermostat
 */
public class ThermostatTest {

    /**
     * Testing method
     */
    @Test
    public void test_Thermostat() throws SQLException, MqttException, PubSubClientException {
        Radiator radiator = new Radiator(1, new Mock_PubSubClient());
        Temperature temp = new Temperature(1, new Mock_PubSubClient());
        Thermostat cut = new Thermostat(1, radiator, temp);


        System.out.println(radiator.getValue());

        // Test set/get Temperature
        cut.setTemperature(21.0);
        assertEquals(cut.getTemperature(), 21.0);

        // set actual temp to 25°C, so the heater turns off
        temp.changeCallback("/Sensors/Sensor_001", "25.0");
        assertEquals(radiator.getValue(), 0.0);

        // set actual temp to 15°C, so the heater turns on
        temp.changeCallback("/Sensors/Sensor_001", "15.0");
        assertEquals(radiator.getValue(), 1.0);

        // Test if On/Off functionality works
        cut.setOff();
        assertEquals(OFF, cut.getPowerState());
        assertEquals(radiator.getValue(), 0.0);

        cut.setOn();
        assertEquals(ON, cut.getPowerState());
        assertEquals(radiator.getValue(), 1.0);
    }

}
