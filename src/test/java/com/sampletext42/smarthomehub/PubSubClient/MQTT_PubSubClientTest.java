package com.sampletext42.smarthomehub.PubSubClient;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit test for MQTT_PubSubClientTest.
 */
@Disabled("Skipping MQTT_PubSubClientTest, see MQTT_PubSubClientTest.java if you really want to test this! [comment @Disabled(..) ] but you need a mqtt-broker (see resources/mqtt.properties for this)")
public class MQTT_PubSubClientTest {
    private CountDownLatch done = new CountDownLatch(1);

    private void changeCallback(String topic, String message) {
        System.out.println(topic + " - " + message);
        this.done.countDown();
    }

    @Test
    public void test_mqtt() throws PubSubClientException, InterruptedException {
        IPubSubClient cut = new MQTT_PubSubClient();

        cut.subscribe("/test/test", 0, this::changeCallback);

        cut.publish("/test/test", "test123", 0, false);

        boolean processComplete = done.await(2, TimeUnit.SECONDS);
        assertEquals(processComplete, true, "job should execute because it is enabled again!");
    }



}
