package com.sampletext42.smarthomehub.Building;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

/**
 * Class for testing building
 */
public class BuildingTest {

    /**
     * Testing method
     * @throws SQLException
     */
    @Test
    public void test_Building_empty() {
        Building cut = new Building();
        assertEquals(cut.getRoomCount(), 0, "Expecting 0 Rooms after initialisation");

    }

    @Test
    public void test_Building_AddRoom() {
        Building cut = new Building();
        cut.addRoom(new Room(-1, "room", 123));

        assertEquals(cut.getRoomCount(), 1, "Expecting 1 Room after adding 1");
    }

    @Test
    public void test_Building_getRoomsOnLevel() {
        Building cut = new Building();
        cut.addRoom(new Room(-1, "room", 123));

        assertEquals(cut.getRoomsOnLevel(123).size(), 1, "Expecting 1 Room on level 123");
        assertEquals(cut.getRoomsOnLevel(0).size(), 0, "Expecting 0 Rooms on level 0");

    }

    @Test
    public void test_Building_getRooms() {
        Building cut = new Building();
        cut.addRoom(new Room(-1, "room", 123));

        assertEquals(cut.getRooms().size(), 1, "Expecting 1 Room in Building");

    }

    @Test
    public void test_Building_getAllFloors() {
        Building cut = new Building();

        cut.addRoom(new Room(-1, "room", 123));
        assertEquals(cut.getAllFloors().size(), 1, "Expecting only 1 Floor in Building");


        cut.addRoom(new Room(-2, "room", 1337));
        assertEquals(cut.getAllFloors().size(), 2, "Expecting now 2 Floors in Building");
    }
}
