package com.sampletext42.smarthomehub.Building;
import com.sampletext42.smarthomehub.Device.BaseDevice.DeviceIO;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class for testing Room
 */
public class RoomTest {

    /**
     * Testing method
     */
    @Test
    public void test_Room() throws SQLException, MqttException {
        Room cut = new Room(1, "myroom", 123);

        assertEquals(cut.getID(), 1, "Expecting ID = 1");
        assertEquals(cut.getLevel(), 123, "Expecting Level = 123");
        assertEquals(cut.getName(), "myroom", "Expecting Name = myroom");
    }

    @Test
    public void test_Room_AddDevice_getDeviceCount_getDevices() throws SQLException, MqttException {
        Room cut = new Room(1, "myroom", 123);

        DeviceIO myDevice = new DeviceIO() {};

        cut.addDevice(myDevice);
        assertEquals(cut.getDeviceCount(), 1, "Expecting 1 Device after adding 1");

        assertTrue(cut.getDevices().get(0) == myDevice);
    }

}
