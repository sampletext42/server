package com.sampletext42.smarthomehub;

import com.sampletext42.smarthomehub.PubSubClient.IPubSubClient;
import com.sampletext42.smarthomehub.PubSubClient.IPubSubMessageListener;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;

import java.util.HashMap;

/**
 * Very very simple mockup for an PubSubClient (wildcards dont't work)
 * 2 functions to one topic don't work
 */
public class Mock_PubSubClient implements IPubSubClient {

    private HashMap<String, IPubSubMessageListener> callbacks = new HashMap<>();


    @Override
    public void publish(String topic, String message, int qos, boolean retained) throws PubSubClientException {
        try {
            IPubSubMessageListener iPubSubMessageListener = callbacks.get(topic);
            iPubSubMessageListener.messageArrived(topic, message);
        } catch (Exception ignored) {}
    }

    @Override
    public void subscribe(String topic, int qos, IPubSubMessageListener runnable) throws PubSubClientException {
        callbacks.put(topic, runnable);
    }
}
