package com.sampletext42.smarthomehub.Mutator;

import static org.junit.jupiter.api.Assertions.*;

import com.sampletext42.smarthomehub.Device.BaseDevice.DeviceIO;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for testing IFTTT
 */
public class IFTTTTest {

    /**
     * Testing method
     */
    @Test
    public void test_IFTTT() {
        Logger logger = LoggerFactory.getLogger(IFTTTTest.class.getName());//LogManager.getLogger(App.class.getName());
        final boolean[] on_on = {false};
        final boolean[] on_off = {false};
        final boolean[] off_on = {false};
        final boolean[] off_off = {false};
        final boolean[] any_on = {false};

        DeviceIO myDevice_this = new DeviceIO() {};
        DeviceIO myDevice_that_1 = new DeviceIO() { // Overriding setOn/setOff function to decrease our CountDownLatch
            @Override
            public void setOn() { on_on[0] = true; System.out.println("1_on"); onChange(); } // ifttt 1
            @Override
            public void setOff() { on_off[0] = true; System.out.println("1_off"); onChange(); } // ifttt 3
        };
        DeviceIO myDevice_that_2 = new DeviceIO() { // Overriding setOn/setOff function to decrease our CountDownLatch
            @Override
            public void setOn() { off_on[0] = true; System.out.println("2_on"); onChange(); } // ifttt 2
            @Override
            public void setOff() { off_off[0] = true; System.out.println("2_off"); onChange(); } // ifttt 4
        };
        DeviceIO myDevice_that_3 = new DeviceIO() { // Overriding setOn/setOff function to decrease our CountDownLatch
            @Override
            public void setOn() { any_on[0] = true; System.out.println("3_on"); onChange(); } // ifttt 5
        };

        // instantiating CUTs
        new IFTTT(1, myDevice_this, IFTTT_condition.SWITCHED_ON, new Operation(1, myDevice_that_1, Operation_Action.SWITCH_ON));
        new IFTTT(2, myDevice_this, IFTTT_condition.SWITCHED_OFF, new Operation(2, myDevice_that_2, Operation_Action.SWITCH_ON));

        new IFTTT(3, myDevice_this, IFTTT_condition.SWITCHED_ON, new Operation(3, myDevice_that_1, Operation_Action.SWITCH_OFF));
        new IFTTT(4, myDevice_this, IFTTT_condition.SWITCHED_OFF, new Operation(4, myDevice_that_2, Operation_Action.SWITCH_OFF));

        new IFTTT(5, myDevice_this, IFTTT_condition.SWITCHED_ANY, new Operation(5, myDevice_that_3, Operation_Action.SWITCH_ON));


        logger.info("All created, asserting all false now/no ifttt triggered #############################");
        assertFalse(on_on[0], "IFTTT This on that on #1");
        assertFalse(off_on[0], "IFTTT This off that on #2");
        assertFalse(on_off[0], "IFTTT This on that off #3");
        assertFalse(off_off[0], "IFTTT This off that off #4");


        logger.info("Switching now on #############################");
        myDevice_this.setOn();
        logger.info("asserting on stuff #############################");
        assertTrue(on_on[0], "IFTTT This on that on #1");
        assertTrue(on_off[0], "IFTTT This on that off #3");
        assertFalse(off_on[0], "IFTTT This off that on #2");
        assertFalse(off_off[0], "IFTTT This off that off #4");
        assertTrue(any_on[0], "IFTTT This any that on #5");


        logger.info("Switching now off #############################");
        myDevice_this.setOff();
        logger.info("asserting everything triggered now #############################");
        assertTrue(on_on[0], "IFTTT This on that on #1");
        assertTrue(off_on[0], "IFTTT This off that on #2");
        assertTrue(on_off[0], "IFTTT This on that off #3");
        assertTrue(off_off[0], "IFTTT This off that off #4");
        assertTrue(any_on[0], "IFTTT This any that on #5"); // redundant
    }
}
