package com.sampletext42.smarthomehub.Mutator;

import static org.junit.jupiter.api.Assertions.*;

import com.sampletext42.smarthomehub.Device.BaseDevice.DeviceIO;
import com.sampletext42.smarthomehub.Device.BaseDevice.PowerState;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

/**
 * Class for testing Operation
 */
public class OperationTest {

    /**
     * Testing method
     */
    @Test
    public void test_Operation() throws SQLException, MqttException {
        DeviceIO myDevice = new DeviceIO() {};
        myDevice.setOff();



        Operation cut = new Operation(1, myDevice, Operation_Action.SWITCH_ON);
        Operation cut2 = new Operation(2, myDevice, Operation_Action.SWITCH_OFF);
        Operation cut3 = new Operation(2, myDevice, Operation_Action.TOGGLE);

        myDevice.setOff();
        cut.trigger();
        assertEquals(myDevice.getPowerState(), PowerState.ON, "switch on trigger");


        myDevice.setOn();
        cut2.trigger();
        assertEquals(myDevice.getPowerState(), PowerState.OFF, "switch off trigger");

        // lamp is now off because of cut2 trigger

        cut3.trigger();
        assertEquals(myDevice.getPowerState(), PowerState.ON, "switch toggle trigger #1");

        cut3.trigger();
        assertEquals(myDevice.getPowerState(), PowerState.OFF, "switch toggle trigger #2");
    }
}
