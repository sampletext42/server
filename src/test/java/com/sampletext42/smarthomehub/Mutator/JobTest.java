package com.sampletext42.smarthomehub.Mutator;

import static org.junit.jupiter.api.Assertions.*;

import com.sampletext42.smarthomehub.Device.BaseDevice.DeviceIO;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Class for testing Job
 */
@Disabled("Skipping JobTest, see JobTest.java if you really want to test this! [comment @Disabled(..) ]")
public class JobTest {

    /**
     * Testing method
     */

    @Test
    public void test_Job() throws InterruptedException {
        System.out.println("THESE TESTS TAKE A FEW SECONDS BECAUSE THEY DEPEND ON REAL CHANGES OF THE SECONDS OF THE SYSTEM TIME!");
        final CountDownLatch done = new CountDownLatch(1);

        DeviceIO myDevice = new DeviceIO() { // Overriding setOn/setOff function to decrease our CountDownLatch
            @Override
            public void setOn() {
                done.countDown();
            }
            @Override
            public void setOff() {
                done.countDown();
            }
        };

        Operation myoperation = new Operation(1, myDevice, Operation_Action.SWITCH_ON);
        Job cut = new Job(1, "0/1 * * * * ?", myoperation);

        boolean processComplete = done.await(2, TimeUnit.SECONDS);
        // true if CountDownLatch get's to 0 (it starts with 0 so needs to decrease one time),
        // false if 2 second timeout is reached.

        assertEquals(processComplete, true, "timeout on job that should run every minute!");

        cut.disable(); // the job would still be executed in all tests after this => so stopping.
    }

    @Test
    public void test_disabled() throws InterruptedException {
        final CountDownLatch done = new CountDownLatch(1);

        DeviceIO myDevice = new DeviceIO() { // Overriding setOn function to decrease our CountDownLatch
            @Override
            public void setOn() {
                done.countDown();
            }
            @Override
            public void setOff() {
                done.countDown();
            }
        };

        Operation myoperation = new Operation(1, myDevice, Operation_Action.SWITCH_ON);
        Job cut = new Job(2, "0/1 * * * * ?", myoperation);
        cut.disable();

        boolean processComplete = done.await(2, TimeUnit.SECONDS);
        assertEquals(processComplete, false, "job should not be execute because it is disabled!");

        //cut.disable(); // the job would still be executed in all tests after this => so stopping.
    }

    @Test
    public void test_disabled_enabled() throws InterruptedException {
        final CountDownLatch done = new CountDownLatch(1);

        DeviceIO myDevice = new DeviceIO() { // Overriding setOn function to decrease our CountDownLatch
            @Override
            public void setOn() {
                done.countDown();
            }
            @Override
            public void setOff() {
                done.countDown();
            }
        };

        Operation myoperation = new Operation(1, myDevice, Operation_Action.SWITCH_ON);
        Job cut = new Job(3, "0/1 * * * * ?", myoperation);
        cut.disable();
        cut.enable();

        boolean processComplete = done.await(2, TimeUnit.SECONDS);
        assertTrue(processComplete, "job should execute because it is enabled again!");

        cut.disable(); // the job would still be executed in all tests after this => so stopping.
    }
}
