package com.sampletext42.smarthomehub.Actuator;

import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Mock_PubSubClient;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Class for testing Light
 */
public class RelayTest {

    /**
     * Testing method
     * @throws SQLException
     */
    @Test
    public void test_Relay() throws PubSubClientException {
        Relay cut = new Relay(1, new Mock_PubSubClient());

        assertEquals(1, cut.getID());

        cut.setValue(1.0);
        assertEquals(1.0, cut.getValue(), 1E-5);

        assertEquals("Actuator(1) = 1.0 (Relay)", cut.toString());
    }

}
