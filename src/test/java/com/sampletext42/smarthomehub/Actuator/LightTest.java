package com.sampletext42.smarthomehub.Actuator;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Mock_PubSubClient;
import org.junit.jupiter.api.Test;

/**
 * Class for testing Light
 */
public class LightTest {

    /**
     * Testing method
     * @throws SQLException
     */
    @Test
    public void test_Light() throws PubSubClientException {
        Light cut = new Light(1, new Mock_PubSubClient());

        assertEquals(1, cut.getID());

        cut.setValue(1.0);
        assertEquals(1.0, cut.getValue(), 1E-5);
        // we could test mqtt here by subscribing to topic and waiting for an answer, but async ;(

        assertEquals("Actuator(1) = 1.0 (Light)", cut.toString());
    }

}
