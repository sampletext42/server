package com.sampletext42.smarthomehub.Actuator;

import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Mock_PubSubClient;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Class for testing Light
 */
public class LightBrightnessTest {

    /**
     * Testing method
     * @throws SQLException
     */
    @Test
    public void test_LightBrightness() throws PubSubClientException {
        LightBrightness cut = new LightBrightness(1, new Mock_PubSubClient());

        assertEquals(1, cut.getID());

        cut.setValue(3.5);
        assertEquals(3.5, cut.getValue(), 1E-5);

        assertEquals("Actuator(1) = 3.5% (LightBrightness)", cut.toString());
    }

}
