package com.sampletext42.smarthomehub.Actuator;

import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Mock_PubSubClient;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Class for testing Light
 */
public class RadiatorTest {

    /**
     * Testing method
     * @throws SQLException
     */
    @Test
    public void test_Radiator() throws PubSubClientException {
        Radiator cut = new Radiator(1, new Mock_PubSubClient());

        assertEquals(1, cut.getID());

        cut.setValue(3.5);
        assertEquals(3.5, cut.getValue(), 1E-5);

        assertEquals("Actuator(1) = 3.5% (Radiator)", cut.toString());
    }

}
