package com.sampletext42.smarthomehub.Building;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Class Building; has levels and Rooms
 * @author Klinksiek
 */

public class Building {

    private static final Logger logger = LoggerFactory.getLogger(Building.class.getName());//LogManager.getLogger(App.class.getName());

    private ArrayList<Room> rooms = new ArrayList<>();

    /**
     * Constructor for Building
     */
    public Building() {
        logger.trace("Building ctor called");
    }

    /**
     * Gets the Rooms of a Building
     * @return Rooms of a Building
     */
    public ArrayList<Room> getRooms() {
        return rooms;
    }

    /**
     * Gets all floor of a Building
     * @return floors
     */
    public HashSet<Integer> getAllFloors() {
        HashSet<Integer> floors = new HashSet<>();

        for (Room room: rooms) {
            floors.add(room.getLevel());
        }
        return floors;
    }

    /**
     * Gets the number of Devices of a Building
     * @return number of Devices
     */
    public int getDeviceCountBuilding(){
        int devCount = 0;

        for (Room room : rooms){
            devCount += room.getDeviceCount();
        }
        return devCount;
    }

    /**
     * Gets the number of Rooms of a Building
     * @return number of Rooms
     */
    public int getRoomCount() {
        return rooms.size();
    }

    /**
     * Gets the Rooms on a specific level of a Building
     * @param level level of the Building
     * @return number of Rooms
     */
    public ArrayList<Room> getRoomsOnLevel(int level) {
        ArrayList<Room> roomsOnLvl = new ArrayList<>();

        for (Room room: rooms) {
            if (room.getLevel() == level) {
                roomsOnLvl.add(room);
            }
        }
        return roomsOnLvl;
    }

    /**
     * Adds a Room to a Building
     * @param newRoom Room to be added
     */
    public void addRoom(Room newRoom) {
        rooms.add(newRoom);
    }

    /**
     * Deletes a Room
     * @param delRoom Room to be deleted
     */
    public void delRoom(Room delRoom){
        if(rooms.contains(delRoom)){
            rooms.remove(delRoom);
        }
        else{
            System.out.println("Room does not exist!");
        }
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("Building");
        return str.toString();
    }

}
