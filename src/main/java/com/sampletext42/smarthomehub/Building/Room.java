package com.sampletext42.smarthomehub.Building;

import com.sampletext42.smarthomehub.Device.BaseDevice.Device;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Class Room; has an id, name, level and Devices
 * @author Klinksiek
 */

public class Room {

    private static final Logger logger = LoggerFactory.getLogger(Room.class.getName());

    private int id;
    private String name;
    private int level;
    private ArrayList<Device> devices = new ArrayList<>();

    /**
     * Constructor of Room
     * @param id id of the room
     * @param name name of the room
     * @param level level of the room
     */
    public Room(int id, String name, int level) {
        this.id = id;
        this.name = name;
        this.level = level;
        logger.trace("Room ctor called");
    }


    /**
     * Gets the id of the the room
     * @return id of room
     */
    public int getID() {
        return this.id;
    }

    /**
     * Gets the name of the room
     * @return name of room
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the level of the room
     * @return level of room
     */
    public int getLevel() {
        return this.level;
    }

    /**
     * Adds a Device to the List
     * @param newDevice Device to be added
     */
    public void addDevice(Device newDevice) {
        devices.add(newDevice);
    }

    /**
     * Gets the number of devices of the room
     * @return number of devices
     */
    public int getDeviceCount() {
        return devices.size();
    }

    /**
     * Gets devices of the room
     * @return devices of a room
     */
    public ArrayList<Device> getDevices() {
        return devices;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Room #");
        sb.append(this.id);
        sb.append(" - '");
        sb.append(this.name);
        sb.append("' on level: ");
        sb.append(this.level);

        return sb.toString();
    }

}
