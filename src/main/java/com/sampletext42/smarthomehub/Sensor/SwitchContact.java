package com.sampletext42.smarthomehub.Sensor;

import com.sampletext42.smarthomehub.PubSubClient.IPubSubClient;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Sensor.BaseSensor.Sensor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Sensor SwitchContact
 * @author Klinksiek
 */

public class SwitchContact extends Sensor {

    private static final Logger logger = LoggerFactory.getLogger(SwitchContact.class.getName());//LogManager.getLogger(App.class.getName());

    /**
     * Constructor for SwitchContact
     * @param id id
     * @param pubsub_client PubSub Client instance
     * @throws PubSubClientException PubSubClient Error
     */
    public SwitchContact(int id, IPubSubClient pubsub_client) throws PubSubClientException {
        super(id, pubsub_client); // for initialising mqtt-client
        this.type = "SwitchContact";
        this.unit="I/O";
        this.id = id;
        this.value = 0.0;
        this.min = 0.0;
        this.max = 1.0;

        // this.value //get's automatically assigned because of the retained MQTT-Message that will arrive like "now"
        logger.debug("  Sensor: Sensor Humidity created #" + id);
    }

}
