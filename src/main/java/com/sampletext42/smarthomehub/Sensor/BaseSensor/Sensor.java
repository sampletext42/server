package com.sampletext42.smarthomehub.Sensor.BaseSensor;

import com.sampletext42.smarthomehub.PubSubClient.IPubSubClient;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

/**
 * Sensor is an abstract superclass for the individual sensors
 * @author Klinksiek
 */

public abstract class Sensor {

    private static final Logger logger = LoggerFactory.getLogger(Sensor.class.getName());//LogManager.getLogger(App.class.getName());

    protected double value;
    protected Double min = Double.NEGATIVE_INFINITY;
    protected Double max = Double.POSITIVE_INFINITY;
    protected int id;
    protected String type;
    protected String unit;

    private IPubSubClient pubsub_client;

    private HashMap<String, Runnable> callbacks = new HashMap<>();

    /**
     * Constructor for Sensor
     * @param id id
     * @param pubsub_client PublishSubscribe client
     * @throws PubSubClientException PubSubClient Error
     */
    public Sensor(int id, IPubSubClient pubsub_client) throws PubSubClientException {
        this.pubsub_client = pubsub_client;//MqttSource.getConnection();
        this.id = id;

        this.pubsub_client.subscribe(String.format("/Sensors/Sensor_%d", id), 1, this::changeCallback);
    }

    /**
     * Call this if any_change get's changed (some Sensor/Actuator Stuff)
     * This Calls then everyone that is interested in Changes on this Device.
     */
    public void onChange() {
        for (Runnable runnable : callbacks.values()) {
            runnable.run();
        }
    }

    /**
     * This Callback get's called every time a message is send in the subscribed topic for this sensor
     * @param topic mqtt-topic for message
     * @param message message
     */
    public void changeCallback(String topic, String message) {
        logger.debug(String.format("Sensor #%d-%s MQTT Message: %s - %s", this.id, this.type, topic, message));
        setValue(Double.valueOf(message));
        this.onChange();
    }

    /**
     * Register a Callback for this Sensor (If any_change on this Sensor changes, it will be called)
     * @param name name for this callback (needs to be unique) (name needed for deleting this callback)
     * @param runnable that get's executed on any change of this Sensor
     */
    public void AddCallback(String name, Runnable runnable) {
        this.callbacks.put(name, runnable);
    }

    /**
     * Unregister a Callback for this Sensor
     * @param name name for this callback
     */
    public void DelCallback(String name) {
        this.callbacks.remove(name);
    }

    /**
     * Gets id of the sensor
     * @return id of sensor
     */
    public int getID() {
        return this.id;
    }

    /**
     * Sets the value of Sensor
     * @param new_val new value for sensor
     */
    protected void setValue(double new_val) {
        if (new_val >= this.min && new_val <= this.max) {
            this.value = new_val;
        } else {
            logger.warn(" Sensor #%i Bad Sensor Value, didn't accept %d", this.id, this.value);
        }
        // else throw exception?
        this.onChange();
    }

    /**
     * Gets the value of Sensor
     * @return value of sensor
     */
    public double getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Sensor(");
        str.append(id);
        str.append(") = ");
        str.append(value);
        str.append(unit);
        str.append(" (");
        str.append(type);
        str.append(")");
        return str.toString();
    }


}

