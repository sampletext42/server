package com.sampletext42.smarthomehub.Sensor;

import com.sampletext42.smarthomehub.PubSubClient.IPubSubClient;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Sensor.BaseSensor.Sensor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Sensor Powermeter
 * @author Klinksiek
 */

public class Powermeter extends Sensor {

    private static final Logger logger = LoggerFactory.getLogger(Temperature.class.getName());//LogManager.getLogger(App.class.getName());

    /**
     * Constructor for Powermeter
     * @param id id
     * @param pubsub_client PubSub Client instance
     * @throws PubSubClientException PubSubClient Error
     */
    public Powermeter(int id, IPubSubClient pubsub_client) throws PubSubClientException {
        super(id, pubsub_client); // for initialising mqtt-client
        this.type = "Powermeter";
        this.unit="W";
        this.id = id;
        this.value = 0.0;
        // this value can be positive or negative, has no limits.

        // this.value //get's automatically assigned because of the retained MQTT-Message that will arrive like "now"
        logger.debug("  Sensor: Sensor Powermeter created #" + id);
    }

}
