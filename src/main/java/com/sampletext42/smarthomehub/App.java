package com.sampletext42.smarthomehub;

import com.sampletext42.smarthomehub.Actuator.*;
import com.sampletext42.smarthomehub.Mutator.*;
import com.sampletext42.smarthomehub.Sensor.*;
import com.sampletext42.smarthomehub.Building.*;
import com.sampletext42.smarthomehub.Device.BaseDevice.DeviceIO;
import com.sampletext42.smarthomehub.Device.*;
import com.sampletext42.smarthomehub.PubSubClient.MQTT_PubSubClient;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.UI.BuildingManagement;

import io.moquette.broker.Server;
import io.moquette.broker.config.ClasspathResourceLoader;
import io.moquette.broker.config.IConfig;
import io.moquette.broker.config.IResourceLoader;
import io.moquette.broker.config.ResourceLoaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * App class implements the "Brain" of the SmartHomeHub
 * @author Frömelt
 */

public class App
{

    private static final Logger logger = LoggerFactory.getLogger(App.class.getName());//LogManager.getLogger(App.class.getName());

    /**
     * Helper Function to decrease code size, creates the operations and Ifttts for a switchable Device
     * @param myDevice Device that should switch according to mySwitch
     * @param mySwitch Switch that should switch myDevice
     * @param roomNumber Room-number to give IFTTTs and Operations the proper id
     */
    public static void add_switch_ifttt(DeviceIO myDevice, Switch mySwitch, int roomNumber) {
        roomNumber *= 1000; // shift roomNumber
        Operation op_switchOn = new Operation(roomNumber + 1, myDevice, Operation_Action.SWITCH_ON);
        Operation op_switchOff = new Operation(roomNumber + 2, myDevice, Operation_Action.SWITCH_OFF);
        new IFTTT(roomNumber + 1, mySwitch, IFTTT_condition.SWITCHED_ON, op_switchOn);
        new IFTTT(roomNumber + 2, mySwitch, IFTTT_condition.SWITCHED_OFF, op_switchOff);
    }

    /**
     * Main Method, is also creating a hard-coded Building
     * @param args args
     * @throws IOException Error of Moquette MQTT Broker
     * @throws PubSubClientException Error of PubSubClient (MQTT)
     */
    public static void main( String[] args ) throws IOException, PubSubClientException {
        logger.info("SMU Server starting.");
        //Server server = Server.createTcpServer(args).start(); // Starting embedded H2 DB Server
        //Runtime.getRuntime().addShutdownHook(new Thread(server::stop, "h2_Shutdown")); // On Exit we need to clean up the server (save db to disk)

        IResourceLoader classpathLoader = new ClasspathResourceLoader("moquette.conf");
        final IConfig classPathConfig = new ResourceLoaderConfig(classpathLoader);

        final Server mqttBroker = new Server();
        mqttBroker.startServer(classPathConfig);
        Runtime.getRuntime().addShutdownHook(new Thread(mqttBroker::stopServer, "mqtt_shutdown"));
        logger.info("Broker started press [CTRL+C] to stop");


        MQTT_PubSubClient psc = new MQTT_PubSubClient();
        Building mybuilding = new Building();

        //TODO: remove hard-coded Building or move it to another function (Later it should be parsed from a file)
        //<editor-fold desc="Building initialisation code">

        // Creating Rooms, starting with Room 001
        Room room001 = new Room(1, "001 Toilet", 0);
        Lamp lamp001001 = new Lamp(1001, new Light(1001, psc));
        Switch switch001001 = new Switch(1002, new SwitchContact(1002, psc));
        add_switch_ifttt(lamp001001, switch001001, 1);
        room001.addDevice(lamp001001);
        room001.addDevice(switch001001);

        // Room 013
        Room room013 = new Room(13, "013 Cafeteria", 0);
        Lamp lamp013001 = new Lamp(13001, new Light(13001, psc));
        //Switch switch01302 = new Switch(13002, new SwitchContact(13002, psc));
        //add_switch_ifttt(lamp001001, switch001001, 1);
        Operation op013_on = new Operation(1, lamp013001, Operation_Action.SWITCH_ON);
        Operation op013_off = new Operation(2, lamp013001, Operation_Action.SWITCH_OFF);
        Job job013_on = new Job(1,  "0 0/1 * ? * * *", op013_on);
        Job job013_off = new Job(2, "30 0/1 * ? * * *", op013_off);
        room013.addDevice(lamp013001);
        //room013.addDevice(switch01302);

        // Room 037
        Room room037 = new Room(37, "037 Office", 0);
        LampwBrightness lampwBrightness37001 = new LampwBrightness(37001, new LightBrightness(37001, psc));
        Switch switch037002 = new Switch(37002, new SwitchContact(37002, psc));
        add_switch_ifttt(lampwBrightness37001, switch037002, 37);
        room037.addDevice(lampwBrightness37001);
        room037.addDevice(switch037002);
        room037.addDevice(new ThermoHygrometer(37003, new Temperature(37003001, psc), new Humidity(37003002, psc)));
        room037.addDevice(new Thermostat(37004, new Radiator(37004001, psc), new Temperature(37004002, psc)));
        room037.addDevice(new SmartSocket(37005, new Relay(37005001, psc), new Powermeter(37005002, psc)));

        // Room 042
        Room room042 = new Room(42, "042 Office", 0);
        LampwBrightness lampwBrightness042001 = new LampwBrightness(42001, new LightBrightness(42001, psc));
        Switch switch042002 = new Switch(42002, new SwitchContact(42002, psc));
        add_switch_ifttt(lampwBrightness042001, switch042002, 42);

        room042.addDevice(lampwBrightness042001);
        room042.addDevice(switch042002);
        room042.addDevice(new ThermoHygrometer(42003, new Temperature(42003001, psc), new Humidity(42003002, psc)));
        room042.addDevice(new Thermostat(42004, new Radiator(42004001, psc), new Temperature(42004002, psc)));
        room042.addDevice(new SmartSocket(42005, new Relay(42005001, psc), new Powermeter(42005002, psc)));

        // Room 103
        Room room103 = new Room(103, "103 Office", 1);
        LampwBrightness lampwBrightness103001 = new LampwBrightness(103001, new LightBrightness(103001, psc));
        Switch switch103002 = new Switch(103002, new SwitchContact(103002, psc));
        add_switch_ifttt(lampwBrightness103001, switch103002, 103);

        room103.addDevice(lampwBrightness103001);
        room103.addDevice(switch103002);
        room103.addDevice(new ThermoHygrometer(103003, new Temperature(103003001, psc), new Humidity(103003002, psc)));
        room103.addDevice(new Thermostat(103004, new Radiator(103004001, psc), new Temperature(103004002, psc)));
        room103.addDevice(new SmartSocket(103005, new Relay(103005001, psc), new Powermeter(103005002, psc)));

        // Room 113
        Room room113 = new Room(113, "113 Cafeteria", 1);
        Lamp lamp113001 = new Lamp(113001, new Light(113001, psc));
        Switch switch113002 = new Switch(113002, new SwitchContact(113002, psc));
        add_switch_ifttt(lamp113001, switch113002, 113);

        room113.addDevice(lamp113001);
        room113.addDevice(switch113002);

        // Room 137
        Room room137 = new Room(137, "137 Lounge", 1);
        LampwBrightness lampwBrightness137001 = new LampwBrightness(137001, new LightBrightness(137001, psc));
        Switch switch137002 = new Switch(137002, new SwitchContact(137002, psc));
        add_switch_ifttt(lampwBrightness137001, switch137002, 137);

        room137.addDevice(lampwBrightness137001);
        room137.addDevice(switch137002);
        room137.addDevice(new ThermoHygrometer(137003, new Temperature(137003001, psc), new Humidity(137003002, psc)));

        // Room 142
        Room room142 = new Room(142, "142 Closet", 1);
        LampwBrightness lampwBrightness142001 = new LampwBrightness(142001, new LightBrightness(142001, psc));
        Switch switch142002 = new Switch(142002, new SwitchContact(142002, psc));
        add_switch_ifttt(lampwBrightness137001, switch137002, 142);

        room142.addDevice(lampwBrightness142001);
        room142.addDevice(switch142002);
        room142.addDevice(new ThermoHygrometer(142003, new Temperature(142003001, psc), new Humidity(142003002, psc)));


        // Adding All Rooms to Building, Erdgeschoss
        mybuilding.addRoom(room001);
        mybuilding.addRoom(room013);
        mybuilding.addRoom(room037);
        mybuilding.addRoom(room042);
        //  1. Etage
        mybuilding.addRoom(room103);
        mybuilding.addRoom(room113);
        mybuilding.addRoom(room137);
        mybuilding.addRoom(room142);
        //</editor-fold>

        BuildingManagement bm = new BuildingManagement(mybuilding);

        while (true) {
            try {
                bm.mainMenu();
            } catch (Exception ex) {
                logger.error("Exception in BuildingManagement!", ex);
                break;
            }
        }
        // collect garbage...
        // oh right, get's done automatically
        System.exit(0);
    }

}
