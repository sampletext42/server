package com.sampletext42.smarthomehub.Actuator;

import com.sampletext42.smarthomehub.Actuator.BaseActuator.Actuator;
import com.sampletext42.smarthomehub.PubSubClient.IPubSubClient;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Actuator Light
 * @author Rodenberg
 */

public class Light extends Actuator {

    private static final Logger logger = LoggerFactory.getLogger(Light.class.getName());//LogManager.getLogger(App.class.getName());

    /**
     * Constructor for Light
     * @param id id
     * @param pubsub_client IPubSubClient
     * @throws PubSubClientException PubSubClient Error
     */
    public Light(int id, IPubSubClient pubsub_client) throws PubSubClientException {
        super(id, pubsub_client);
        this.type = "Light";
        this.unit=""; // I/O halt
        this.id = id;
        this.value = 0.0;
        this.min = 0.0;
        this.max = 1.0;

        logger.debug("  Actuator: Actuator Light created #" + id);
    }

}
