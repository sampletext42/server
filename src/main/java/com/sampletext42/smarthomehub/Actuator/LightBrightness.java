package com.sampletext42.smarthomehub.Actuator;

import com.sampletext42.smarthomehub.Actuator.BaseActuator.Actuator;
import com.sampletext42.smarthomehub.PubSubClient.IPubSubClient;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Actuator LightBrightness
 * @author Rodenberg
 */

public class LightBrightness extends Actuator {

    private static final Logger logger = LoggerFactory.getLogger(LightBrightness.class.getName());//LogManager.getLogger(App.class.getName());

    /**
     * Constructor for LightBrightness
     * @param id id
     * @param pubsub_client IPubSubClient
     * @throws PubSubClientException PubSubClient Error
     */
    public LightBrightness(int id, IPubSubClient pubsub_client) throws PubSubClientException {
        super(id, pubsub_client);
        this.type = "LightBrightness";
        this.unit="%"; // I/O halt
        this.id = id;
        this.value = 0.0;
        this.min = 0.0;
        this.max = 100.0;

        logger.debug("  Actuator: Actuator LightBrightness created #" + id);
    }
}
