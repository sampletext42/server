package com.sampletext42.smarthomehub.Actuator.BaseActuator;

import com.sampletext42.smarthomehub.PubSubClient.IPubSubClient;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

/**
 * Actuator is an abstract superclass for individual Actuators
 * @author Rodenberg
 */

public abstract class Actuator {

    private static final Logger logger = LoggerFactory.getLogger(Actuator.class.getName());//LogManager.getLogger(App.class.getName());

    protected double value;
    protected Double min = Double.NEGATIVE_INFINITY;
    protected Double max = Double.POSITIVE_INFINITY;
    protected int id;
    protected String type;
    protected String unit;

    private IPubSubClient pubsub_client;

    private HashMap<String, Runnable> callbacks = new HashMap<>();


    /**
     * Constructor for Actuator
     * @param id Actuator id
     * @param pubsub_client PublishSubscribe client
     * @throws PubSubClientException PubSubClient Error
     */
    public Actuator(int id, IPubSubClient pubsub_client) throws PubSubClientException {
        this.id = id;
        this.pubsub_client = pubsub_client;
    }

    /**
     * Call this if any_change get's changed (some Sensor/Actuator Stuff)
     * This Calls then everyone that is interested in Changes on this Device.
     */
    public void onChange() {
        for (Runnable runnable : callbacks.values()) {
            runnable.run();
        }
    }

    /**
     * Register a Callback for this Actuator (If any_change on this Actuator changes, it will be called)
     * @param name name for this callback (needs to be unique) (name needed for deleting this callback)
     * @param runnable that get's executed on any change of this Actuator
     */
    public void AddCallback(String name, Runnable runnable) {
        this.callbacks.put(name, runnable);
    }

    /**
     * Unregister a Callback for this Actuator
     * @param name name for this callback
     */
    public void DelCallback(String name) {
        this.callbacks.remove(name);
    }

    /**
     * Gets id of Actuator
     * @return id of Actuator
     */
    public int getID() {
        return this.id;
    }

    /**
     * Sets the value of Actuator
     * @param new_val new value for Actuator
     * @throws PubSubClientException PubSubClient Error
     */
    public void setValue(double new_val) throws PubSubClientException {
        if (new_val >= this.min && new_val <= this.max) {
            this.value = new_val;
        } else {
            logger.warn(" Actuator #%i Bad Actuator Value, didn't accept %d", this.id, this.value);
        }
        pubsub_client.publish(String.format("/Actuators/Actuator_%d", this.id),
                Double.toString(this.value), 1, true); // prod => retained = true
        // Set MySQL Stuff
        this.onChange();
    }

    /**
     * Gets value of Actuator
     * @return value of Actuator
     */
    public double getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("");
        str.append("Actuator(");
        str.append(this.id);
        str.append(") = ");
        str.append(this.value);
        str.append(this.unit);
        str.append(" (");
        str.append(this.type);
        str.append(")");
        return str.toString();
    }

}
