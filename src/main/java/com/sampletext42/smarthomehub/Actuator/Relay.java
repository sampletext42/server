package com.sampletext42.smarthomehub.Actuator;

import com.sampletext42.smarthomehub.Actuator.BaseActuator.Actuator;
import com.sampletext42.smarthomehub.PubSubClient.IPubSubClient;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Actuator Relay
 * @author Rodenberg
 */

public class Relay extends Actuator {

    private static final Logger logger = LoggerFactory.getLogger(Relay.class.getName());//LogManager.getLogger(App.class.getName());

    /**
     * Constructor for Relay
     * @param id id
     * @param pubsub_client IPubSubClient
     * @throws PubSubClientException PubSubClient Error
     */
    public Relay(int id, IPubSubClient pubsub_client) throws PubSubClientException {
        super(id, pubsub_client);
        this.type = "Relay";
        this.unit=""; // I/O halt
        this.id = id;
        this.value = 0.0;
        this.min = 0.0;
        this.max = 1.0;

        logger.debug("  Actuator: Actuator Relay created #" + id);
    }

}
