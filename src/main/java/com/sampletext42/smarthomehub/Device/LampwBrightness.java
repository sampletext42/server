package com.sampletext42.smarthomehub.Device;

import com.sampletext42.smarthomehub.Actuator.LightBrightness;
import com.sampletext42.smarthomehub.Device.BaseDevice.DeviceIO;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Device Lamp with Brightness
 * @author Marten
 */

public class LampwBrightness extends DeviceIO {

    private static final Logger logger = LoggerFactory.getLogger(LampwBrightness.class.getName());//LogManager.getLogger(App.class.getName());

    private double brightness = 1.0;

    /**
     * Constructor for LampwBrightness
     * @param id id
     * @param light LightBrightness
     */
    public LampwBrightness(int id, LightBrightness light) {
        this.type = "LampwBrightness";
        this.id = id;
        this.actuators.add(light);
        logger.debug(" Device: Device LampwBrightness created #" + this.id);
    }

    /**
     * Sets the brightness of the LampwBrightness
     * @param brightness brightness of the Lamp
     */
    public void setBrightness(double brightness){
        this.brightness = brightness;
        try {
            this.actuators.get(0).setValue(brightness);
        } catch (PubSubClientException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the Brightness of the LampwBrightness
     * @return brightness of the LampwBrightness
     */
    public double getBrightness(){
        return this.brightness;
    }

    @Override
    public void setOff() {
        super.setOff();
        try {
            this.actuators.get(0).setValue(0.0);
        } catch (PubSubClientException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOn() {
        super.setOn();
        try {
            this.actuators.get(0).setValue(brightness);
        } catch (PubSubClientException e) {
            e.printStackTrace();
        }
    }

}
