package com.sampletext42.smarthomehub.Device;

import com.sampletext42.smarthomehub.Actuator.Radiator;
import com.sampletext42.smarthomehub.Device.BaseDevice.DeviceIO;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Sensor.Temperature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Device Thermostat
 * @author Marten
 */

public class Thermostat extends DeviceIO {

    private static final Logger logger = LoggerFactory.getLogger(Thermostat.class.getName());//LogManager.getLogger(App.class.getName());

    private double temperature;

    /**
     * Constructor for Thermostat
     * @param id id
     * @param radiator Radiator
     * @param temperature Temperature
     */
    public Thermostat(int id, Radiator radiator, Temperature temperature) {
        this.type = "Thermostat";
        this.id = id;
        this.actuators.add(radiator);
        this.sensors.add(temperature);
        logger.debug(" Device: Device Thermostat created #" + this.id);
        temperature.AddCallback("Thermostat_" + this.id, this::sensorCallback);
    }

    /**
     * This function get's called every time the temp sensor changes, this implements the logic behind the thermostat.
     * (turns heater on if temp below set point, if not turns it off)
     */
    public void sensorCallback() {
        double actual_temp = this.sensors.get(0).getValue();
        try {
            if (actual_temp < this.temperature) {
                this.actuators.get(0).setValue(1.0);
            } else {
                this.actuators.get(0).setValue(0.0);
            }
        } catch (PubSubClientException ex) {
            logger.error("Thermostat error", ex);
        }
    }

    /**
     * Sets the temperature of Thermostat
     * @param temperature temperature
     */
    public void setTemperature(double temperature){
        this.temperature = temperature;
    }

    /**
     * Gets the value of the Thermostat
     * @return temperature of Thermostat
     */
    public double getTemperature(){
        return this.temperature;
    }

    @Override
    public void setOff() {
        super.setOff();
        try {
            this.actuators.get(0).setValue(0.0);
        } catch (PubSubClientException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOn() {
        super.setOn();
        try {
            this.actuators.get(0).setValue(1.0);
        } catch (PubSubClientException e) {
            e.printStackTrace();
        }
    }

}
