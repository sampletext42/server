package com.sampletext42.smarthomehub.Device;

import com.sampletext42.smarthomehub.Actuator.Relay;
import com.sampletext42.smarthomehub.Device.BaseDevice.DeviceIO;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import com.sampletext42.smarthomehub.Sensor.Powermeter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Device SmartSocket
 * @author Marten
 */

public class SmartSocket extends DeviceIO {

    private static final Logger logger = LoggerFactory.getLogger(SmartSocket.class.getName());//LogManager.getLogger(App.class.getName());

    /**
     * Constructor for SmartSocket
     * @param id id
     * @param relay Relay
     * @param powermeter Powermeter
     */
    public SmartSocket(int id, Relay relay, Powermeter powermeter) {
        this.type = "SmartSocket";
        this.id = id;
        this.actuators.add(relay);
        this.sensors.add(powermeter);
        logger.debug(" Device: Device SmartSocket created #" + this.id);
    }

    /**
     * Gets the power usage
     * @return power usage
     */
    public double getPowerUsage() {
        return this.sensors.get(0).getValue();
    }

    @Override
    public void setOff() {
        super.setOff();
        try {
            this.actuators.get(0).setValue(0.0);
        } catch (PubSubClientException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOn() {
        super.setOn();
        try {
            this.actuators.get(0).setValue(1.0);
        } catch (PubSubClientException e) {
            e.printStackTrace();
        }
    }

}
