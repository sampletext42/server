package com.sampletext42.smarthomehub.Device.BaseDevice;

/**
 * Enum for PowerState, used as state for DeviceIO
 * @author Marten
 */
public enum PowerState {

    ON(true),
    OFF(false);

    boolean powerState;

    /**
     * Constructor for PowerState
     * @param powerState Enum PowerState(ON or OFF)
     */
    PowerState(boolean powerState){
        this.powerState = powerState;
    }

}
