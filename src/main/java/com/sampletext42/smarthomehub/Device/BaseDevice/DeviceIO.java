package com.sampletext42.smarthomehub.Device.BaseDevice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.sampletext42.smarthomehub.Device.BaseDevice.PowerState.*;

/**
 * Class used for subclasses with attribute PowerState
 * @author Marten
 */
public abstract class DeviceIO extends Device{

    private static final Logger logger = LoggerFactory.getLogger(Device.class.getName());//LogManager.getLogger(App.class.getName());

    private PowerState powerState = PowerState.OFF;

    /**
     * Sets the powerState to ON(true)
     */
    public void setOn(){
        setPowerState(PowerState.ON);
        logger.debug("   setOn " + toString());
    }

    /**
     * Sets the powerState to OFF(false)
     */
    public void setOff(){
        setPowerState(PowerState.OFF);
        logger.debug("   setOff " + toString());
    }

    /**
     * Sets the powerState
     * @param state the new powerState
     */
    public void setPowerState(PowerState state) {
        this.powerState = state;
        this.onChange();
    }

    /**
     * Gets the powerState of the DeviceIO
     * @return powerState of the DeviceIO
     */
    public PowerState getPowerState(){
        return this.powerState;
    }

    @Override
    public String toString() {
        return "DeviceIO{" +
                "powerState=" + powerState +
                ", id=" + id +
                ", type='" + type + '\'' +
                ", sensors=" + sensors +
                ", actuators=" + actuators +
                '}';
    }

}
