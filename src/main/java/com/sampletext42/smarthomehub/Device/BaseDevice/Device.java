package com.sampletext42.smarthomehub.Device.BaseDevice;

import com.sampletext42.smarthomehub.Actuator.BaseActuator.Actuator;
import com.sampletext42.smarthomehub.Sensor.BaseSensor.Sensor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Abstract class Device; Abstract superclass for different kinds of Devices
 * @author Marten
 */

public abstract class Device {

    private static final Logger logger = LoggerFactory.getLogger(Device.class.getName());//LogManager.getLogger(App.class.getName());

    protected int id;
    protected String type = "Abstract";
    protected String unit;

    protected ArrayList<Sensor> sensors = new ArrayList<>();
    protected ArrayList<Actuator> actuators = new ArrayList<>();

    private HashMap<String, Runnable> callbacks = new HashMap<>();

    /**
     * Gets the id
     * @return id
     */
    public int getID() {
        return this.id;
    }


    public String getType() {
        return this.type;
    }

    /**
     * Call this if any_change get's changed (some Sensor/Actuator Stuff)
     * This Calls then everyone that is interested in Changes on this Device.
     */
    public void onChange() {
        for (Runnable runnable : callbacks.values()) {
            runnable.run();
        }
    }

    /**
     * Register a Callback for this Device (If any_change on this Device changes, it will be called)
     * @param name name for this callback (needs to be unique) (name needed for deleting this callback)
     * @param runnable that get's executed on any change of this device
     */
    public void AddCallback(String name, Runnable runnable) {
        logger.debug("  Device Add callback " + name);
        this.callbacks.put(name, runnable);
    }

    /**
     * Unregister a Callback for this Device
     * @param name name for this callback
     */
    public void DelCallback(String name) {
        this.callbacks.remove(name);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Device #");
        sb.append(this.id);
        sb.append(" - ");
        sb.append(this.type);

        return sb.toString();
    }

}
