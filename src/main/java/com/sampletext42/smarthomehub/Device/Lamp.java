package com.sampletext42.smarthomehub.Device;

import com.sampletext42.smarthomehub.Actuator.Light;
import com.sampletext42.smarthomehub.Device.BaseDevice.DeviceIO;
import com.sampletext42.smarthomehub.PubSubClient.PubSubClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Device Lamp
 * @author Marten
 */

public class Lamp extends DeviceIO {

    private static final Logger logger = LoggerFactory.getLogger(Lamp.class.getName());//LogManager.getLogger(App.class.getName());

    /**
     * Constructor for Lamp
     * @param id id
     * @param light Light
     */
    public Lamp(int id, Light light) {
        this.type = "Lamp";
        this.id = id;
        this.actuators.add(light);
        logger.debug(" Device: Device Lamp created #" + this.id);
    }

    @Override
    public void setOff() {
        super.setOff();
        try {
            this.actuators.get(0).setValue(0.0);
        } catch (PubSubClientException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOn() {
        super.setOn();
        try {
            this.actuators.get(0).setValue(1.0);
        } catch (PubSubClientException e) {
            e.printStackTrace();
        }
    }

}
