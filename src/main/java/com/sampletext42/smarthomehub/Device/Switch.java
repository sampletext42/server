package com.sampletext42.smarthomehub.Device;

import com.sampletext42.smarthomehub.Device.BaseDevice.DeviceIO;
import com.sampletext42.smarthomehub.Sensor.SwitchContact;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Device Switch
 * @author Marten
 */

public class Switch extends DeviceIO {

    private static final Logger logger = LoggerFactory.getLogger(Switch.class.getName());//LogManager.getLogger(App.class.getName());

    /**
     * Constructor for Switch
     * @param id id
     * @param switchContact SwitchContact
     */
    public Switch(int id, SwitchContact switchContact) {
        this.type = "Switch";
        this.id = id;
        this.sensors.add(switchContact);
        logger.debug(" Device: Device Switch created #" + this.id);

        this.sensors.get(0).AddCallback("Switch_" + this.id, this::sensorChangeCallback);
    }

    /**
     * Changes callback for Switch
     */
    private void sensorChangeCallback() {
        if (this.sensors.get(0).getValue() > 1E-5) {
            logger.debug("Switch #" + this.id + " switched on");
            this.setOn();
        }
        else {
            logger.debug("Switch #" + this.id + " switched off");
            this.setOff();
        }
    }

}
