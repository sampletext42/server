package com.sampletext42.smarthomehub.Device;

import com.sampletext42.smarthomehub.Device.BaseDevice.Device;
import com.sampletext42.smarthomehub.Sensor.Humidity;
import com.sampletext42.smarthomehub.Sensor.Temperature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Device ThermoHygrometer
 * @author Marten
 */

public class ThermoHygrometer extends Device {

    private static final Logger logger = LoggerFactory.getLogger(ThermoHygrometer.class.getName());//LogManager.getLogger(App.class.getName());

    private double humidity;
    private double temperature;


    /**
     * Constructor for ThermoHygrometer
     * @param id id
     * @param temperature Temperature
     * @param humidity Humidity
     */
    public ThermoHygrometer(int id, Temperature temperature, Humidity humidity) {
        this.type = "ThermoHygrometer";
        this.id = id;
        this.sensors.add(temperature);
        this.sensors.add(humidity);
        logger.debug(" Device: Device ThermoHygrometer created #" + this.id);
        temperature.AddCallback("ThermoHygrometer_" + this.id, this::sensorCallback);
        humidity.AddCallback("ThermoHygrometer_" + this.id, this::sensorCallback);
    }

    /**
     * Updates values whenever they get changed
     */
    private void sensorCallback() {
        this.temperature = this.sensors.get(0).getValue();
        this.humidity = this.sensors.get(1).getValue();
    }


    /**
     * Gets the temperature of the Device
     * @return temperature of ThermoHygrometer
     */
    public double getTemperature(){
        return this.temperature;
    }

    /**
     * Gets the humidity of the Device
     * @return humidity of ThermoHygrometer
     */
    public double getHumidity(){
        return this.humidity;
    }

}
