package com.sampletext42.smarthomehub.Mutator;

import com.coreoz.wisp.Scheduler;
import com.coreoz.wisp.schedule.cron.CronSchedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Job for a Mutator
 * @author Frömelt
 */

public class Job {

    private static final Logger logger = LoggerFactory.getLogger(Job.class.getName());//LogManager.getLogger(App.class.getName());

    private int id;
    private String cronString; // quartz cron string
    private Operation operation;

    private Scheduler scheduler;
    private com.coreoz.wisp.Job job;

    /**
     * Constructor for Job
     * @param id id
     * @param cronString cron String (Quartz notation) for when this Job should run
     * @param operation operation that will be executed
     */
    public Job(int id, String cronString, Operation operation) {
        this.scheduler = new Scheduler();
        this.id = id;
        this.cronString = cronString;
        this.operation = operation;

        this.enable();
    }

    /**
     * Enables Job
     */
    public void enable() {
        try { // assure that no job get's executed 2 times!
            this.disable();
        } catch (Exception ex) {
            //pass, we actually expect that this happens.
        }
        this.job = scheduler.schedule(String.valueOf(this.id), this::timerCallback, CronSchedule.parseQuartzCron(this.cronString));
    }

    /**
     * Disables Job
     */
    public void disable() {
        scheduler.cancel(this.job.name());
    }

    /**
     * Callback that get's executed on the specific moment that is defined in cronString
     */
    private void timerCallback() {
        logger.debug("Job #" + this.id + " called");
        this.operation.trigger();
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
