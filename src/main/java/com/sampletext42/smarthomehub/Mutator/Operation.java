package com.sampletext42.smarthomehub.Mutator;

import com.sampletext42.smarthomehub.Device.BaseDevice.DeviceIO;
import com.sampletext42.smarthomehub.Device.BaseDevice.PowerState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Operation for a Mutator (Operation mutates an Devices Powerstate if triggered)
 * @author Frömelt
 */

public class Operation {

    private static final Logger logger = LoggerFactory.getLogger(Operation.class.getName());//LogManager.getLogger(App.class.getName());

    private int id;
    private Operation_Action op;
    private DeviceIO device;

    /**
     * Constructor for Operation
     * @param id id
     * @param device DeviceIO
     * @param op Operation_Action
     */
    public Operation(int id, DeviceIO device, Operation_Action op) {
        this.id = id;
        this.device = device;
        this.op = op;

        logger.trace("Operation ctor called for" + this.id);
    }

    /**
     * Triggers an event (get's called from the outside, triggers *this* event)
     */
    public void trigger() {
        switch (this.op) {
            case SWITCH_ON:
                logger.trace("Operation #" + this.id + "trigger called to " + this.op.name());
                trigger_switch_on();
                break;
            case SWITCH_OFF:
                logger.trace("Operation #" + this.id + "trigger called to " + this.op.name());
                trigger_switch_off();
                break;
            case TOGGLE:
                logger.trace("Operation #" + this.id + "trigger called to " + this.op.name());
                trigger_switch_toggle();
                break;
        }
    }

    /**
     * Trigger Switch on Device
     */
    private void trigger_switch_on() {
        DeviceIO tmpDevice = this.device;
        logger.debug("  Operation #" + this.id + " switch_on - " + this.device);
        tmpDevice.setOn();
    }

    /**
     * Trigger Switch off Device
     */
    private void trigger_switch_off() {
        logger.debug("  Operation #" + this.id + " switch_off - " + this.device);
        DeviceIO tmpDevice = this.device;
        tmpDevice.setOff();
    }

    /**
     * Trigger Toggle Device
     */
    private void trigger_switch_toggle() {
        logger.debug("  Operation #" + this.id + " switch_toggle - " + this.device);
        DeviceIO tmpDevice = this.device;

        if (tmpDevice.getPowerState() == PowerState.OFF) {
            logger.debug("  toggle to on");
            tmpDevice.setOn();
        } else {
            logger.debug("  toggle to off");
            tmpDevice.setOff();
        }
    }


    @Override
    public String toString() {
        return super.toString();
    }

}
