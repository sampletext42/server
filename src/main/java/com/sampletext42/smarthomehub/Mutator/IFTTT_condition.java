package com.sampletext42.smarthomehub.Mutator;

/**
 * Enum IFTTT_condition, specifies what need's to happen to trigger an IFTTT
 * @author Frömelt
 */

public enum IFTTT_condition {

    SWITCHED_ON,
    SWITCHED_OFF,
    SWITCHED_ANY

}
