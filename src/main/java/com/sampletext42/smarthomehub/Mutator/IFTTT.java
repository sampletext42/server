package com.sampletext42.smarthomehub.Mutator;

import com.sampletext42.smarthomehub.Device.BaseDevice.Device;
import com.sampletext42.smarthomehub.Device.BaseDevice.DeviceIO;
import com.sampletext42.smarthomehub.Device.BaseDevice.PowerState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/***
 * Class IFTTT(If-This-Than-That)
 * @author Frömelt
 */

public class IFTTT {

    private static final Logger logger = LoggerFactory.getLogger(IFTTT.class.getName());//LogManager.getLogger(App.class.getName());

    private int id;

    private Device thisDevice;
    private IFTTT_condition thi;
    private Operation operation;

    /**
     * Constructor for IFTTT
     * @param id id
     * @param thisDevice thisDevice
     * @param thi IFTTT_condition
     * @param operation Operation
     */
    public IFTTT(int id, Device thisDevice, IFTTT_condition thi, Operation operation) {
        this.id = id;
        this.thisDevice = thisDevice;
        this.thi = thi;
        this.operation = operation;

        this.thisDevice.AddCallback("IFTTT_"+this.id, this::this_callback);
        logger.debug("  IFTTT: IFTTT #" + this.id);
    }

    /**
     * Enables this IFTTT-instance
     */
    public void enable() {
        this.thisDevice.AddCallback("IFTTT_"+this.id, this::this_callback);
    }

    /**
     * Disables this IFTTT-instance
     */
    public void disable() {
        this.thisDevice.DelCallback("IFTTT_"+this.id);
    }

    /**
     * Triggers the Operation
     */
    private void this_callback() {
        DeviceIO tmpDevice = (DeviceIO) this.thisDevice;
        PowerState ps = tmpDevice.getPowerState();
        if (
                (this.thi == IFTTT_condition.SWITCHED_ON && ps == PowerState.ON) |
                (this.thi == IFTTT_condition.SWITCHED_OFF && ps == PowerState.OFF) |
                (this.thi == IFTTT_condition.SWITCHED_ANY)) {
            logger.debug(" IFTTT #" + this.id + " this happened ");
            this.operation.trigger();
        }
    }

}
