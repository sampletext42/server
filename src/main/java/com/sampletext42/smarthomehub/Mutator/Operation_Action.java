package com.sampletext42.smarthomehub.Mutator;

/**
 * Enum for Operation_Action, "Operation_Action" specifies what should happen (so if Device should be switched on/off or toggle).
 * @author Frömelt
 */

public enum Operation_Action {

    SWITCH_ON,
    SWITCH_OFF,
    TOGGLE

}
