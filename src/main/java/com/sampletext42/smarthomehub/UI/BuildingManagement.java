package com.sampletext42.smarthomehub.UI;

import com.mitchtalmadge.asciidata.table.ASCIITable;
import com.sampletext42.smarthomehub.Building.Building;
import com.sampletext42.smarthomehub.Building.Room;

import com.sampletext42.smarthomehub.Device.BaseDevice.Device;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.Scanner;

/**
 * Interface of the project
 *@author Marten
 */

public class BuildingManagement {

    private static final Logger logger = LoggerFactory.getLogger(BuildingManagement.class.getName());//LogManager.getLogger(App.class.getName());

    private Scanner sc = new Scanner(System.in);
    private Building building;

    private String header = "BuildingManagement\n" +
            "##################\n" +
            "Choose an option:\n" +
            "[ 0] Exit\n" +
            "[ 1] Print Building status\n" +
            "[ 2] List all floor numbers\n" +
            "[ 3] List all rooms\n" +
            "[ 4] List rooms of floor\n" +
            "[ 5] List Devices of Room\n";/* +
            "[ 6] Coming soon!\n" + //maybe del a Dev
            "[ 7] Coming soon!\n" + //maybe del a Room
            "[ 8] Coming soon!\n" + //maybe del a floor
            "[ 9] Coming soon!\n"*/;  //dont know


    /**
     * Constructor for Building
     * @param building building
     */
    public BuildingManagement(Building building){
        this.building = building;
    }



    /**
     * Gets the user input as an int (with a costum prompt)
     * @param prompt input
     * @return input as Integer
     */
    private int getIntInput(String prompt) {
        System.out.print(prompt + " ");
        int val = sc.nextInt();
        sc.nextLine(); // Remove rest of the input (for accidental inputs etc.)
        return val;
    }

    /**
     * Gets the user input as an int (with a ">> " as prompt)
     * @return input as Integer
     */
    private int getIntInput() {
        return getIntInput(">>");
    }

    /**
     * Gets the user input as a String
     * @param prompt input
     * @return input as String
     */
    private String getStringInput(String prompt) {
        System.out.print(prompt + " ");
        return sc.nextLine();
    }

    /**
     * Gets the user input as a String
     * @return input as String
     */
    private String getStringInput() {
        return getStringInput(">> ");
    }


    /**
     * Displays the main menu
     */
    public void mainMenu() {
        System.out.println("\n" + header);
        execMenuOption(getIntInput());
    }

    /**
     * Prints all levels of a building
     */
    private void listAllLevels() {
        //System.out.print("List of all Floors: ");
        //System.out.println(this.building.getAllFloors());
        String[] headers = new String[] {"Floor numbers"};
        String[][] data = new String[building.getAllFloors().size()][1];

        int i=0;
        for (int floor : building.getAllFloors()) {
            data[i][0] = String.valueOf(floor);
            i++;
        }
        System.out.println(ASCIITable.fromData(headers, data).toString());
    }

    /**
     * Lists all rooms of a building
     */
    private void listRooms() {
        String[] headers = new String[] {"Number", "Name", "Level", "# of Devices"};
        String[][] data = new String[building.getRooms().size()][4];

        int i = 0;
        for (Room room : building.getRooms()) {
            data[i][0] = String.valueOf(room.getID());
            data[i][1] = String.valueOf(room.getName());
            data[i][2] = String.valueOf(room.getLevel());
            data[i][3] = String.valueOf(room.getDeviceCount());
            i++;
        }
        System.out.println(ASCIITable.fromData(headers, data).toString());
    }

    /**
     * Lists all room for a specific floor of a building
     * @param floor level of the building
     */
    private void listRooms(int floor) {
        String[] headers = new String[] {"Number", "Name", "Level", "# of Devices"};
        String[][] data = new String[building.getRoomsOnLevel(floor).size()][4];

        int i = 0;
        for (Room room : building.getRoomsOnLevel(floor)) {
            data[i][0] = String.valueOf(room.getID());
            data[i][1] = String.valueOf(room.getName());
            data[i][2] = String.valueOf(room.getLevel());
            data[i][3] = String.valueOf(room.getDeviceCount());
            i++;
        }
        System.out.println(ASCIITable.fromData(headers, data).toString());
    }

    /**
     * Lists all Devices of a specific Room
     * @param room Room from which Devices will be listed
     */
    private void listDevicesOfRoom(Room room) {
        //System.out.println(room.getDevices());
        String[] headers = new String[] {"Number", "Type"};
        String[][] data = new String[room.getDeviceCount()][2];

        int i = 0;
        for (Device device : room.getDevices()) {
            data[i][0] = String.valueOf(device.getID());
            data[i][1] = String.valueOf(device.getType());
            i++;
        }
        System.out.println(ASCIITable.fromData(headers, data).toString());
    }

    /**
     * Check's if a floor number exists in the building
     * @param floor number
     * @return true if floor exists, else false
     */
    private boolean _existsFloor(int floor) {
        return this.building.getAllFloors().contains(floor);
    }

    /**
     * Let's the user pick a Floor and returns it (int)
     */
    private int pickFloor() {
        System.out.println("Select a Floor from one of the following: ");
        listAllLevels();

        int floor = getIntInput();

        if (!_existsFloor(floor)) { // Check if floor exists, if not try again
            System.out.println(String.format("Floor %d does not exist, pick another one!", floor));
            return pickFloor();
        }

        return floor; // floor exists so return it
    }

    /**
     * Function that let's a User select a Room and returns the corresponding instance
     * (only used internally?)
     */
    private Room pickRoom() {

        int floor = pickFloor();

        listRooms(floor);
        int roomid = getIntInput("Pick a room: >> ");

        for (Room room: building.getRoomsOnLevel(floor)) {
            if (room.getID() == roomid) {
                return room;
            }
        }
        System.out.println("room id not found, try again");
        return pickRoom();
    }

    /**
     * Prints general information
     */
    private void printBuildingStatus(){
        System.out.println("Building status: ");
        System.out.println("=========");

        String[] headers = new String[] {"Floors", "# of rooms", "# of devices"};
        String[][] data = new String[][] {
                {String.valueOf(building.getAllFloors()),
                 String.valueOf(building.getRoomCount()),
                 String.valueOf(building.getDeviceCountBuilding())}
        };
        System.out.println(ASCIITable.fromData(headers, data).toString());
    }

    /**
     * Executes menu options
     *  // note: bad design, will change (maybe...)
     * @param option number of chosen option
     */
    private void execMenuOption (int option) {
        System.out.println();
        try {
            switch (option) {
                case 0: // Exit
                    System.exit(1);
                    break;
                case 1:
                    printBuildingStatus();
                    break;
                case 2:
                    listAllLevels();
                    break;
                case 3:
                    listRooms();
                    break;
                case 4:
                    listRooms(pickFloor());
                    break;
                case 5:
                    listDevicesOfRoom(pickRoom());
                    break;
                    /*
                    TODO: on/off ifttt
                    TODO: on/off job
                     */
                case 6:

                case 7:
                default:
                    System.out.println("This option does not exist ;(");
            }
        } catch (Exception ex) {
            logger.error("Exception on execMenuOption ", ex);
        }
        System.out.println();
    }






}
