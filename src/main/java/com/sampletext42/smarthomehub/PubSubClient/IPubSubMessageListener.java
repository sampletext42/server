package com.sampletext42.smarthomehub.PubSubClient;

/**
 * Interface IPubSubClient, used for different implementations of the method messageArrived
 * This is the callback function anyone needs to implement to subscribe to any topic.
 * @author Frömelt
 */

public interface IPubSubMessageListener {

    /**
     * messageArrived gets called every time a message arrives in the subsrcibed topic
     * @param topic topic
     * @param message message
     * @throws Exception Error
     */
    void messageArrived(String topic, String message) throws Exception;

}
