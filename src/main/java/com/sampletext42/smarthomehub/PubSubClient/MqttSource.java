package com.sampletext42.smarthomehub.PubSubClient;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;
import java.util.UUID;

/**
 * Class for instantiating a MQTT-client that is used in various places in our project
 * @author Frömelt
 */

public class MqttSource {

    private static final String config_filename = "mqtt.properties";
    private static final MqttConnectOptions options;

    private static String mqtt_broker_uri;
    private static String mqtt_broker_usr;
    private static String mqtt_broker_passwd;

     // Reads the mqtt.properties file and create's a corresponding MQTT-options instance.
    static {
        try {
            getConfig();
        } catch (Exception ex) {
            System.out.println("Failed to load MQTT config!");
            System.out.println(ex);
            System.exit(-1);
        }

        options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(3);
        options.setUserName(mqtt_broker_usr);
        options.setPassword(mqtt_broker_passwd.toCharArray());

        String[] uris = {mqtt_broker_uri};
        options.setServerURIs(uris);
    }

    /**
     * This function returns the MQTT-options that got created on startup
     * @return mqttConnect options
     */
    public static MqttConnectOptions getOptions() {
        return options;
    }

    /**
     * This function returns a MQTT-client instance that is already set up and connected to the server
     * @return client
     * @throws MqttException Mqtt Error
     */
    public static IMqttAsyncClient getConnection() throws MqttException {
        IMqttAsyncClient client = new MqttAsyncClient(options.getServerURIs()[0], UUID.randomUUID().toString(), new MemoryPersistence());
        client.connect(options).waitForCompletion(); // wait for succesful connection to server
        return client;
    }


    /**
     * Load the Configuration Variables from File.
     * {@link #config_filename} is read.
     * @throws IOException In/Output Error
     */
    private static void getConfig() throws IOException {
        // load file then load properties from file
        String cfgPath = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource(config_filename)).getPath();
        Properties p = new Properties();
        p.load(new FileInputStream(cfgPath));

        // check if all properties are there
        if (p.getProperty("mqtt_broker_uri")==null ||p.getProperty("mqtt_broker_usr")==null ||p.getProperty("mqtt_broker_passwd")==null) {
            //logger.error("getConfig failed!");
            throw new IOException("missing properties in mqtt.properties!");
        }

        // importing properties
        mqtt_broker_uri = p.getProperty("mqtt_broker_uri");
        mqtt_broker_usr = p.getProperty("mqtt_broker_usr");
        mqtt_broker_passwd = p.getProperty("mqtt_broker_passwd");
    }

}
