package com.sampletext42.smarthomehub.PubSubClient;

/**
 * Interface IPubSubClient, used for different implementations of the methods publish and subscribe
 * (so it implements a publish-subscribe message pattern)
 * @author Frömelt
 */
public interface IPubSubClient {

    //void connect(); // concrete class ctor should return a completely configured and connected instance.

    /**
     * Publishes a Message to a topic
     * @param topic topic to be published to
     * @param message message
     * @param qos qos
     * @param retained retained
     * @throws PubSubClientException PubSubClient Error
     */
    void publish(String topic, String message, int qos, boolean retained) throws PubSubClientException;

    /**
     * Subscribes to a topic
     * @param topic topic to subscribe to
     * @param qos qos
     * @param runnable IPubSubMessageListener runnable which should be called if Message arrived in topic
     * @throws PubSubClientException PubSubClient Error
     */
    void subscribe(String topic, int qos, IPubSubMessageListener runnable) throws PubSubClientException;

}
