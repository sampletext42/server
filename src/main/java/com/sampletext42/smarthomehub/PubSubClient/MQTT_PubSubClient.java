package com.sampletext42.smarthomehub.PubSubClient;

import org.eclipse.paho.client.mqttv3.IMqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 * Class MQTT_PubSubClient
 * @author Frömelt
 */
public class MQTT_PubSubClient implements IPubSubClient{

    private IMqttAsyncClient mqtt_client;

    /**
     * Implements IPubSubClient for concrete MQTT-Client.
     * @throws PubSubClientException PubSubClient Error
     */
    public MQTT_PubSubClient() throws PubSubClientException {
        try {
            this.mqtt_client = MqttSource.getConnection();

            Runtime.getRuntime().addShutdownHook(new Thread(() -> { // On Exit we need to clean up the mqtt-connection!
                try {
                    this.mqtt_client.disconnect();
                    this.mqtt_client.close();
                } catch (MqttException ignored) {}
            }));

        } catch (MqttException ex) {
            throw new PubSubClientException("Couldn't create MQTT connection!", ex);
        }
    }

    /**
     * MQTT publish function
     * @param topic topic to be published to
     * @param message message
     * @param qos qos
     * @param retained retained
     * @throws PubSubClientException
     */
    @Override
    public void publish(String topic, String message, int qos, boolean retained) throws PubSubClientException {
        try {
            this.mqtt_client.publish(topic, message.getBytes(), qos, retained);
        } catch (MqttException ex) {
            throw new PubSubClientException("Error on publishing MQTT message!", ex);
        }
    }

    /**
     * MQTT subscribe function
     * @param topic topic to subscribe to
     * @param qos qos
     * @param runnable IPubSubMessageListener runnable which should be called if Message arrived in topic
     * @throws PubSubClientException
     */
    @Override
    public void subscribe(String topic, int qos, IPubSubMessageListener runnable) throws PubSubClientException {
        try {
            this.mqtt_client.subscribe(topic, qos, (_topic ,_message) -> {
                String tmp_msg = new String( _message.getPayload() );
                runnable.messageArrived(_topic, tmp_msg);
            });
        } catch (MqttException ex) {
            throw new PubSubClientException("Error on subscribing to MQTT topic!", ex);
        }
    }

}
