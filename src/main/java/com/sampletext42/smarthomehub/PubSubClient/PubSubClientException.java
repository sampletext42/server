package com.sampletext42.smarthomehub.PubSubClient;

/**
 * Class PubSubClientException used for custom Exceptions
 * @author Frömelt
 */
public class PubSubClientException extends Exception{

    /**
     * Constructor for PubSubClientException
     * @param message message
     */
    public PubSubClientException(String message) {
        super(message);
    }

    /**
     * Constructor for PubSubClientException
     * @param cause cause of Exception
     */
    public PubSubClientException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor for PubSubClientException
     * @param message message
     * @param cause cause of Exception
     */
    public PubSubClientException(String message, Throwable cause) {
        super(message, cause);
    }

}
