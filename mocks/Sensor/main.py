#Author Maximilian Frömelt

import os
import paho.mqtt.client as mqtt
from colorama import init, Fore, Back, Style
init() # colorama



mhost="127.0.0.1"
musr="smhsmh"
mpw="SuppenTopfKartoffelHack33"

mqttc = mqtt.Client(client_id="Sensor")
mqttc.username_pw_set(musr, mpw)
mqttc.connect(host=mhost, port=1883, keepalive=60)


sensor_id = None
sensor_value = 0.0

cls = lambda: os.system("cls" if os.name == "nt" else "clear")

def cprint(str, clr=Style.NORMAL):
	print(clr + str + Style.RESET_ALL)

def cinput(str, clr=Style.NORMAL):
	return input(clr + str + Style.RESET_ALL)


def myexit():
	print("exiting.")
	mqttc.disconnect()
	exit()
	
def header():
	return  """
Virtual Sensor
  sensor_id : %s
  sensor_val: %s
""" % (sensor_id, sensor_value)
	 

def set_val():
	global sensor_value
	
	sensor_value = float(input("New sensor value: "))
	mqttc.publish("/Sensors/Sensor_%03d" % sensor_id, sensor_value, qos=1, retain=True)
	
def set_id():
	global sensor_id
	sensor_id = int(input("New sensor id: "))

	
	
	
menuItems = [ {"Exit": myexit},
			  {"Set value": set_val}, 
			  {"Set new sensor id": set_id} ]




def main():
	pass
	while True:
		cls()
		cprint(header(), Fore.BLUE)
		
		for indx, itm in enumerate(menuItems):
			cprint("[%s] %s" % (indx, list(itm.keys())[0]) , Fore.GREEN)
		print("")
		
		try:
			choice = cinput(">> ", Fore.YELLOW)
		except KeyboardInterrupt:
			myexit()
		
		try:
			if int(choice) < 0:
				raise ValueError
			list(menuItems[int(choice)].values())[0]()
		except (ValueError, IndexError, KeyboardInterrupt):
			pass

def enterInfo():
	global sensor_id
	try:
		sensor_id = int(input("SensorID: "))
	except:
		exit(print("error!"))

enterInfo()
main()
myexit()
