#!/usr/bin/python3
#Author Maximilian Frömelt

import os
import paho.mqtt.client as mqtt
from colorama import init, Fore, Back, Style
init() # colorama

import datetime#
import threading


mhost="127.0.0.1"
musr="smhsmh"
mpw="SuppenTopfKartoffelHack33"

mqttc = mqtt.Client(client_id="Actuator")
mqttc.username_pw_set(musr, mpw)
mqttc.connect(host=mhost, port=1883, keepalive=60)
mqttc.loop_start()

actuator_id = None
actuator_value = 0.0
actuator_last_update = datetime.datetime(1970, 1, 1, 0, 0, 0, 0)

cls = lambda: os.system("cls" if os.name == "nt" else "clear")
usr_lock = threading.Lock()


def cprint(str, clr=Style.NORMAL, _end="\n"):
	print(clr + str + Style.RESET_ALL, end=_end)


def myexit():
	cprint("exiting.")
	mqttc.disconnect()
	exit()
	
def header():
	return  """
Virtual actuator
  Actuator_id : %s
  Atuator_val: %s
  Last update: %s
""" % (actuator_id, actuator_value, actuator_last_update.strftime('%Y-%m-%d %H:%M:%S'))
	 
	
def set_id():
	global actuator_id, actuator_value, actuator_last_update
	tmp_actuator_id = int(input("New Actuator id: "))
	actuator_value = 0.0
	actuator_last_update = datetime.datetime(1970, 1, 1, 0, 0, 0, 0)
	
	try:
		mqttc.unsubscribe("/Actuators/Actuator_%03d" % actuator_id)
	except:
		pass
	
	tmp = "/Actuators/Actuator_%03d" % tmp_actuator_id
	cprint(tmp)
	mqttc.subscribe(tmp)
	actuator_id = tmp_actuator_id
	
	
def refresh():
	pass


def on_message(client, userdata, message):
	global actuator_value, actuator_last_update
	
	#print("message received " ,str(message.payload.decode("utf-8")))
	
	actuator_last_update = datetime.datetime.now()
	
	actuator_value = float(str(message.payload.decode("utf-8")))
	

    #print("message topic=",message.topic)
    #print("message qos=",message.qos)
    #print("message retain flag=",message.retain)
	acquired = usr_lock.acquire(0)
	if acquired:
		print_fullheader()
	else:
		cprint("New data, please refresh!", Fore.RED)

	if acquired:
		usr_lock.release()


def print_items():
	for indx, itm in enumerate(menuItems):
		cprint("[%s] %s" % (indx, list(itm.keys())[0]) , Fore.GREEN)
	cprint("")

def print_fullheader():
	cls()
	cprint(header(), Fore.BLUE)
	print_items()
	cprint(">> ", Fore.YELLOW, _end="") # input

menuItems = [ {"Exit": myexit},
			  {"Set new actuator id": set_id}, 
			  {"Refresh": refresh}]


def main():
	pass
	while True:
		print_fullheader()
		
		try:
			choice = input()
		except KeyboardInterrupt:
			myexit()
		
		try:
			usr_lock.acquire()
			if int(choice) < 0:
				raise ValueError

			list(menuItems[int(choice)].values())[0]()
		except (ValueError, IndexError, KeyboardInterrupt):
			pass
		finally:
			usr_lock.release()

def enterInfo():
	global actuator_id
	try:
		set_id() #actuator_id = int(input("actuatorID: "))
	except:
		exit(cprint("error!"))

mqttc.on_message=on_message        #attach function to callback
enterInfo()
main()
myexit()
