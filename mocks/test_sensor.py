#!/usr/bin/python3
import paho.mqtt.client as mqtt
import uuid
import time
import random
import datetime
import os

mhost="127.0.0.1"
musr="smhsmh"
mpw="SuppenTopfKartoffelHack33"

sensor_id = int(input("New Sensor id: "))
sensor_value = 0.0
sensor_last_update = datetime.datetime(1970, 1, 1, 0, 0, 0, 0)

cls = lambda: os.system("cls" if os.name == "nt" else "clear")

def on_message(client, userdata, msg):
    global sensor_value, sensor_last_update
    print(msg.topic+" "+str(msg.payload))
    try:
        sensor_last_update = datetime.datetime.now()
        sensor_value = float(str(msg.payload.decode("utf-8")))
        pretty_print()
    except Exception as ex:
        print("Exception in on_message!")
        print(ex)

def on_connect(client, userdata, flags, rc):
    #print("Connected with result code "+str(rc))
    print("Connection attempt returned: " + mqtt.connack_string(rc)) # mqtt !!
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    tmp = "/Sensors/Sensor_%d" % sensor_id
    client.subscribe(tmp, qos=1)
    print("subscribed to %s\n" % tmp)

def pretty_print():
    l = len(str(sensor_id))
    dt = sensor_last_update.strftime('%Y-%m-%d %H:%M:%S')
    l2 = len(dt)

    cls()

    print("+" + "="*(l+2) + "+")
    print("| %i |" % sensor_id)
    print("+" + "="*(l+2) + "+")


    print("+" + "="*(l2+2) + "+")
    print("| %s |" % dt)
    print("+" + "="*(l2+2) + "+")

    siz = 10
    ch = "#"
    if sensor_value < 0.5:
        ch = " "

    print("+" + "="*(siz*2) + "+")
    for i in range(siz):
        print("|%s|" % (ch*siz*2))
    print("+" + "="*(siz*2) + "+")

    print("\n[0] set off\n[1] set on\n")


    

mqttc = mqtt.Client(client_id="Switch" + str(uuid.uuid4().hex)[0:14])
mqttc.username_pw_set(musr, mpw)

mqttc.on_message=on_message
mqttc.on_connect=on_connect

mqttc.connect(host=mhost, port=1883, keepalive=60)


# Blocking call that processes network traffic, dispatches callbacks etc
pretty_print()
mqttc.loop_start()
while True:
    try:
        tmp_n = int(input())
        if tmp_n == 0:
            tmp_sensor_val = 0.0
        elif tmp_n == 1:
            tmp_sensor_val = 1.0
        else:
            raise Exception()
        print("pub sens_val: %s" % str(tmp_sensor_val))
        mqttc.publish("/Sensors/Sensor_%d" % sensor_id, tmp_sensor_val, qos=1, retain=True)
    except:
        print("Exception in mainmenu")
        exit()
