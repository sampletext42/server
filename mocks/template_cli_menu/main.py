#Author Maximilian Frömelt

import os
from colorama import init, Fore, Back, Style
init() # colorama


def cprint(str, clr=Style.NORMAL):
	print(clr + str + Style.RESET_ALL)

def cinput(str, clr=Style.NORMAL):
	return input(clr + str + Style.RESET_ALL)


def myexit():
	print("exiting.")
	exit()
def header():
	return "\nVirtual Something\n"


def bar():
	print("bar called")
	input("Press [Enter] to continue")
	

def foo():
	print("foo called")
	input("Press [Enter] to continue")
	
	
	
menuItems = [ {"Exit": myexit},
			  {"Call foo": foo}, 
			  {"Call bar": bar}]




def main():
	pass
	while True:
		os.system("cls")
		cprint(header(), Fore.BLUE)
		
		for indx, itm in enumerate(menuItems):
			cprint("[%s] %s" % (indx, list(itm.keys())[0]) , Fore.GREEN)
		print("")
		
		try:
			choice = cinput(">> ", Fore.YELLOW)
		except KeyboardInterrupt:
			myexit()
		
		try:
			if int(choice) < 0:
				raise ValueError
			list(menuItems[int(choice)].values())[0]()
		except (ValueError, IndexError, KeyboardInterrupt):
			pass


main()
myexit()