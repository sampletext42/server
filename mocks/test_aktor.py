#!/usr/bin/python3
import paho.mqtt.client as mqtt
import uuid
import time
import random
import datetime
import os

mhost="127.0.0.1"
musr="smhsmh"
mpw="SuppenTopfKartoffelHack33"

actuator_id = int(input("New Actuator id: "))
actuator_value = 0.0
actuator_last_update = datetime.datetime(1970, 1, 1, 0, 0, 0, 0)

cls = lambda: os.system("cls" if os.name == "nt" else "clear")

def on_message(client, userdata, msg):
    global actuator_value, actuator_last_update
    print(msg.topic+" "+str(msg.payload))
    try:
        actuator_last_update = datetime.datetime.now()
        actuator_value = float(str(msg.payload.decode("utf-8")))
        pretty_print()
    except Exception as ex:
        print("Exception in on_message!")
        print(ex)

def on_connect(client, userdata, flags, rc):
    #print("Connected with result code "+str(rc))
    print("Connection attempt returned: " + mqtt.connack_string(rc)) # mqtt !!
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    tmp = "/Actuators/Actuator_%d" % actuator_id
    client.subscribe(tmp, qos=1)
    print("subscribed to %s\n" % tmp)

def pretty_print():
    l = len(str(actuator_id))
    dt = actuator_last_update.strftime('%Y-%m-%d %H:%M:%S')
    l2 = len(dt)

    cls()

    print("+" + "="*(l+2) + "+")
    print("| %i |" % actuator_id)
    print("+" + "="*(l+2) + "+")


    print("+" + "="*(l2+2) + "+")
    print("| %s |" % dt)
    print("+" + "="*(l2+2) + "+")

    siz = 10
    ch = "#"
    if actuator_value < 0.5:
        ch = " "

    print("+" + "="*(siz*2) + "+")
    for i in range(siz):
        print("|%s|" % (ch*siz*2))
    print("+" + "="*(siz*2) + "+")


    

mqttc = mqtt.Client(client_id="Lamp" + str(uuid.uuid4().hex)[0:14])
mqttc.username_pw_set(musr, mpw)

mqttc.on_message=on_message
mqttc.on_connect=on_connect

mqttc.connect(host=mhost, port=1883, keepalive=60)


# Blocking call that processes network traffic, dispatches callbacks etc
pretty_print()
mqttc.loop_forever()

