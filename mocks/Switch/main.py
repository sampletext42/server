#Author Maximilian Frömelt

import os
import paho.mqtt.client as mqtt
from colorama import init, Fore, Back, Style
init() # colorama



mhost="127.0.0.1"
musr="smhsmh"
mpw="SuppenTopfKartoffelHack33"

mqttc = mqtt.Client(client_id="Switch")
mqttc.username_pw_set(musr, mpw)
mqttc.connect(host=mhost, port=1883, keepalive=60)


sensor_id = None
sensor_value = 0.0

cls = lambda: os.system("cls" if os.name == "nt" else "clear")



def cprint(str, clr=Style.NORMAL):
	print(clr + str + Style.RESET_ALL)

def cinput(str, clr=Style.NORMAL):
	return input(clr + str + Style.RESET_ALL)


def myexit():
	cprint("exiting.")
	mqttc.disconnect()
	exit()
	
def header():
	tmp = "off"
	if sensor_value > 0:
		tmp = "on"
	return  """
Virtual Switch
  switch_id : %s
  switch_val: %s
""" % (sensor_id, tmp)
	 

def _set_val(val):
	global sensor_value
	sensor_value = float(val)
	mqttc.publish("/Sensors/Sensor_%03d" % sensor_id, sensor_value, qos=1, retain=True)


def set_on():
	_set_val(1.0)

def set_off():
	_set_val(0.0)

def set_toggle():
	if sensor_value > 0:
		_set_val(0.0)
	else:
		_set_val(1.0)

def set_id():
	global sensor_id
	sensor_id = int(input("New sensor id: "))

	
	
	
menuItems = [ {"Exit": myexit},
			  {"Set on": set_on}, 
			  {"Set off": set_off}, 
			  {"Toggle": set_toggle}, 
			  {"Set new switch id": set_id} ]




def main():
	pass
	while True:
		cls()
		cprint(header(), Fore.BLUE)
		
		for indx, itm in enumerate(menuItems):
			cprint("[%s] %s" % (indx, list(itm.keys())[0]) , Fore.GREEN)
		print("")
		
		try:
			choice = cinput(">> ", Fore.YELLOW)
		except KeyboardInterrupt:
			myexit()
		
		try:
			if int(choice) < 0:
				raise ValueError
			list(menuItems[int(choice)].values())[0]()
		except (ValueError, IndexError, KeyboardInterrupt):
			pass

def enterInfo():
	global sensor_id
	try:
		sensor_id = int(input("SensorID: "))
	except:
		exit(print("error!"))

enterInfo()
main()
myexit()
